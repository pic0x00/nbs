//Document ready section
$(document).ready(function(){
//    $(".grid tr:not(.grid)").hide();
//    $(".grid tr:first-child").show();
//    $(".grid tr.grid").click(function(){
//        $(this).nextAll("tr").toggle(500);
//    });
    window.onpopstate=function(){location.reload();}
    $("#maintable a").on("click", function(event){event.preventDefault();});
    $(".back a").on("click", function(event){event.preventDefault();});
    $(window).scroll(FixBack);
    FixBack();
//Init table filter lists
    var slate = new List('tablesearch_slate',options);
    var ondulin = new List('tablesearch_ondulin',options);
    var rabica = new List('tablesearch_rabica',options);
    var fasad = new List('tablesearch_fasad',options);
    var emali = new List('tablesearch_emali',options);
    var kraski = new List('tablesearch_kraski',options);
    var cement25 = new List('tablesearch_cement25',options);
    var cement50 = new List('tablesearch_cement50',options);
    var cdprofil = new List('tablesearch_cdprofil',options);
    var udprofil = new List('tablesearch_udprofil',options);
    var utepl1 = new List('tablesearch_utepl1',options);
    var utepl2 = new List('tablesearch_utepl2',options);
    var sip1 = new List('tablesearch_sip1',options);
    var sip2 = new List('tablesearch_sip2',options);
    var polimin = new List('tablesearch_polimin',options);
    var knaufceresit = new List('tablesearch_knaufceresit',options);
    var doors = new List('tablesearch_doors',options);
    var wallpappers = new List('tablesearch_wallpappers',options);
});
//Filter Table
var options={valueNames:['goodsname','spec']};

function FixBack() {
    var $cache = $(".back");
    if ($(window).scrollTop() > 215)
        $cache.css({'position': 'fixed',
                    "top":"0",
                    "width":"750px",
                    "text-align": "center"
            });
    else
        $cache.css({'position': 'static'});
}

//Not efficient approach but it works
function Modalclose () {
        $("#modal").delay(300).hide("fade",500);
        $("#modalmap").delay(300).hide("fade", 500);
        $("#modalmapclose").delay(300).hide("fade",500);
    }

//ModalMap function
function ShowModalMap (){
    $("#modal").delay(300).toggle("fade",500);
    $("#modalmap").delay(300).toggle("fade", 500);
    $("#modalmapclose").delay(300).toggle("fade",500);
}

//Comeback to main menu section
function GetMenu (){
    $("#information").hide("fade", 300);
    $("#maintable").delay(300).show("fade",500);
    history.pushState("","","index.html");
    $(document).find("title").load("index.html", function(){document.title=$(this).find("title").text();});
}

//Action menu //            $(".grid tr.grid").on("click",function(){$(this).nextAll("tr").fadeToggle(500);}).eq(0).trigger('click');
function ChangeInf (e) {
    function Execution (page){
        var target=page;
        history.pushState("", "", target);
        $(document).find("title").load(target, function(){document.title=$(this).find("title").text();});
        $("#maintable").hide("fade", 500);
        $("#information").load(target+" "+"#information.inf");
        $("#information").delay(500).show("fade", 500);
    }
    switch (e) {
        case "block1": Execution("1.html");break;
        case "block2": Execution("2.html");break;
        case "block3": Execution("3.html");break;
        case "block4": Execution("4.html");break;
        case "block5": Execution("5.html");break;
        case "block6": Execution("6.html");break;
        case "block7": Execution("7.html");break;
        case "block8": Execution("8.html");break;
        case "block9": Execution("9.html");break;
    }
}




